// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";


interface IDefi {
    function initialize(address, address, uint, uint, address) external;
}

contract DefiStaking is AccessControl, IDefi, Ownable {
    using SafeMath for uint256;

    string public constant NAME = "Defi Staking Contract";
    bytes32 public constant REWARD_PROVIDER = keccak256("REWARD_PROVIDER"); // i upgraded solc and used REWARD_PROVIDER instead of whitelist role and DEFAULT_ADMIN_ROLE instead of whiteloist admin
    uint256 private constant TIME_UNIT = 86400;
    /* we can improve this with a "unstaked:false" flag when the user force withdraws the funds
     so he can collect the reward later */
    struct Stake {
        uint256 _amount;
        uint256 _timestamp;
        string _packageName;
        uint256 _withdrawnTimestamp;
        uint16 _stakeRewardType; // 0 for native token reward which gives CI, 1 for other stake reward
    }

    struct YieldType {
        string _packageName;
        uint256 _daysLocked;
        uint256 _daysBlocked;
        uint256 _packageInterest;
        uint256 _packageDefiReward; // the number of Defi token received for each native token staked
    }

    IERC20 public tokenContract;
    IERC20 public Defi;

    string[] public packageNames;
    uint256 decimals = 18;
    mapping(string => YieldType) public packages;
    mapping(address => uint256) public totalStakedBalance;
    mapping(address => Stake[]) public stakes;
    mapping(address => bool) public hasStaked;
    address private admin;
    address[] stakers;
    uint256 rewardProviderTokenAllowance = 0;
    uint256 public totalStakedFunds = 0;
    uint256 DefiRewardUnits = 1000000; // ciq reward for 1.000.000 tokens staked
    bool public paused = false;

    event NativeTokenRewardAdded(address indexed _from, uint256 _val);
    event NativeTokenRewardRemoved(address indexed _to, uint256 _val);
    event StakeAdded(
        address indexed _usr,
        string _packageName,
        uint256 _amount,
        uint16 _stakeRewardType,
        uint256 _stakeIndex
    );
    event Unstaked(address indexed _usr, uint256 stakeIndex);
    event ForcefullyWithdrawn(address indexed _usr, uint256 stakeIndex);
    event Paused();
    event Unpaused();

    modifier onlyRewardProvider() {
        require(
            hasRole(REWARD_PROVIDER, _msgSender()),
            "caller does not have the REWARD_PROVIDER role"
        );
        _;
    }

    modifier onlyMaintainer() {
        require(
            hasRole(DEFAULT_ADMIN_ROLE, _msgSender()),
            "caller does not have the Maintainer role"
        );
        _;
    }

    modifier onlyOwnerAndAdmin() {
        require(
            owner() == _msgSender() || _msgSender() == admin,
            "Ownable: caller is not the owner or admin"
        );
        _;
    }

    address factory;
    constructor() {
        factory = msg.sender;
    }

    function stakesLength(address _address) external view returns (uint256) {
        return stakes[_address].length;
    }

    function packageLength() external view returns (uint256) {
        return packageNames.length;
    }

    function stakeTokens(
        uint256 _amount,
        string memory _packageName,
        uint16 _stakeRewardType
    ) public {
        uint256 currentTime = block.timestamp;
        require(currentTime<=endDateTime, "Pool expired");
        require(paused == false, "Staking is  paused");
        require(_amount > 0, " stake a positive number of tokens ");
        require(
            packages[_packageName]._daysLocked > 0,
            "there is no staking package with the declared name, or the staking package is poorly formated"
        );
        require(
            _stakeRewardType == 0 || _stakeRewardType == 1,
            "reward type not known: 0 is native token, 1 is Defi"
        );

        //add to stake sum of address
        totalStakedBalance[msg.sender] = totalStakedBalance[msg.sender].add(
            _amount
        );

        //add to stakes
        Stake memory currentStake;
        currentStake._amount = _amount;
        currentStake._timestamp = block.timestamp;
        currentStake._packageName = _packageName;
        currentStake._stakeRewardType = _stakeRewardType;
        currentStake._withdrawnTimestamp = 0;
        stakes[msg.sender].push(currentStake);

        //if user is not declared as a staker, push him into the staker array
        if (!hasStaked[msg.sender]) {
            stakers.push(msg.sender);
        }

        //update the bool mapping of past and current stakers
        hasStaked[msg.sender] = true;
        totalStakedFunds = totalStakedFunds.add(_amount);
        uint256 amount = _amount * 1e9;
        //transfer from (need allowance)
        tokenContract.transferFrom(msg.sender, address(this), amount);

        emit StakeAdded(
            msg.sender,
            _packageName,
            _amount,
            _stakeRewardType,
            stakes[msg.sender].length - 1
        );

    }

    function checkStakeReward(address _address, uint256 stakeIndex)
        public
        view
        returns (uint256 yieldReward, uint256 timeDiff)
    {
        require(
            stakes[_address][stakeIndex]._stakeRewardType == 0,
            "use checkStakeDefiReward for stakes accumulating reward in Defi"
        );

        uint256 currentTime = block.timestamp;
        if (stakes[_address][stakeIndex]._withdrawnTimestamp != 0) {
            currentTime = stakes[_address][stakeIndex]._withdrawnTimestamp;
        }

        uint256 stakingTime = stakes[_address][stakeIndex]._timestamp;
        uint256 daysLocked =
            packages[stakes[_address][stakeIndex]._packageName]._daysLocked;
        uint256 packageInterest =
            packages[stakes[_address][stakeIndex]._packageName]
                ._packageInterest;

        timeDiff = currentTime.sub(stakingTime).div(TIME_UNIT);

        uint256 yieldPeriods = timeDiff.div(daysLocked); 

        yieldReward = 0;
        uint256 totalStake = stakes[_address][stakeIndex]._amount;

        // for each period of days defined in the package, compound the interest
        while (yieldPeriods > 0) {
            uint256 currentReward = totalStake.mul(packageInterest).div(100);

            totalStake = totalStake.add(currentReward);

            yieldReward = yieldReward.add(currentReward);

            yieldPeriods--;
        }
    }

    function checkStakeDefiReward(address _address, uint256 stakeIndex)
        public
        view
        returns (uint256 yieldReward, uint256 timeDiff)
    {
        require(
            stakes[_address][stakeIndex]._stakeRewardType == 1,
            "use checkStakeReward for stakes accumulating reward in the Native Token"
        );

        uint256 currentTime = block.timestamp;
        if (stakes[_address][stakeIndex]._withdrawnTimestamp != 0) {
            currentTime = stakes[_address][stakeIndex]._withdrawnTimestamp;
        }

        uint256 stakingTime = stakes[_address][stakeIndex]._timestamp;
        uint256 daysLocked =
            packages[stakes[_address][stakeIndex]._packageName]._daysLocked;
        uint256 packageDefiInterest =
            packages[stakes[_address][stakeIndex]._packageName]
                ._packageDefiReward;

        timeDiff = currentTime.sub(stakingTime).div(TIME_UNIT);

        uint256 yieldPeriods = timeDiff.div(daysLocked);

        yieldReward = stakes[_address][stakeIndex]._amount.mul(
            packageDefiInterest
        );

        yieldReward = yieldReward.div(DefiRewardUnits);

        yieldReward = yieldReward.mul(yieldPeriods);
    }

    function unstake(uint256 stakeIndex) public {
        require(
            stakeIndex < stakes[msg.sender].length,
            "The stake you are searching for is not defined"
        );
        require(
            stakes[msg.sender][stakeIndex]._withdrawnTimestamp == 0,
            "Stake already withdrawn"
        );

        // decrease total balance
        totalStakedFunds = totalStakedFunds.sub(
            stakes[msg.sender][stakeIndex]._amount
        );

        //decrease user total staked balance
        totalStakedBalance[msg.sender] = totalStakedBalance[msg.sender].sub(
            stakes[msg.sender][stakeIndex]._amount
        );

        //close the staking package (fix the withdrawn timestamp)
        stakes[msg.sender][stakeIndex]._withdrawnTimestamp = block.timestamp;

        if (stakes[msg.sender][stakeIndex]._stakeRewardType == 0) {
            (uint256 reward, uint256 daysSpent) =
                checkStakeReward(msg.sender, stakeIndex);

            require(
                rewardProviderTokenAllowance > reward,
                "Token creators did not place enough liquidity in the contract for your reward to be paid"
            );

            require(
                daysSpent >
                    packages[stakes[msg.sender][stakeIndex]._packageName]
                        ._daysBlocked,
                "cannot unstake sooner than the blocked time"
            );

            rewardProviderTokenAllowance = rewardProviderTokenAllowance.sub(
                reward
            );

            uint256 totalStake = stakes[msg.sender][stakeIndex]._amount.add(reward).mul(1e9);

            tokenContract.transfer(msg.sender, totalStake);
        } else if (stakes[msg.sender][stakeIndex]._stakeRewardType == 1) {
            (uint256 DefiReward, uint256 daysSpent) =
                checkStakeDefiReward(msg.sender, stakeIndex);

            require(
                Defi.balanceOf(address(this)) >= DefiReward,
                "the isn't enough Defi in this contract to pay your reward right now"
            );

            require(
                daysSpent >
                    packages[stakes[msg.sender][stakeIndex]._packageName]
                        ._daysBlocked,
                "cannot unstake sooner than the blocked time time"
            );

            Defi.transfer(msg.sender, DefiReward);
            uint256 amount = stakes[msg.sender][stakeIndex]._amount * 1e9;
            tokenContract.transfer(
                msg.sender,amount
            );
        } else {
            revert();
        }

        emit Unstaked(msg.sender, stakeIndex);
    }

    function forceWithdraw(uint256 stakeIndex) public {
        require(
            stakes[msg.sender][stakeIndex]._amount > 0,
            "The stake you are searching for is not defined"
        );
        require(
            stakes[msg.sender][stakeIndex]._withdrawnTimestamp == 0,
            "Stake already withdrawn"
        );

        stakes[msg.sender][stakeIndex]._withdrawnTimestamp = block.timestamp;
        totalStakedFunds = totalStakedFunds.sub(
            stakes[msg.sender][stakeIndex]._amount
        );
        totalStakedBalance[msg.sender] = totalStakedBalance[msg.sender].sub(
            stakes[msg.sender][stakeIndex]._amount
        );

        uint256 daysSpent =
            block.timestamp.sub(stakes[msg.sender][stakeIndex]._timestamp).div(
                TIME_UNIT
            ); //86400

        require(
            daysSpent >
                packages[stakes[msg.sender][stakeIndex]._packageName]
                    ._daysBlocked,
            "cannot unstake sooner than the blocked time time"
        );

        tokenContract.transfer(
            msg.sender, stakes[msg.sender][stakeIndex]._amount *1e9
        );

        emit ForcefullyWithdrawn(msg.sender, stakeIndex);
    }

    function pauseStaking() public onlyOwnerAndAdmin {
        paused = true;
        emit Paused();
    }

    function unpauseStaking() public onlyOwnerAndAdmin {
        paused = false;
        emit Unpaused();
    }

    function addStakedTokenReward(uint256 _amount)
        public
        onlyRewardProvider
        returns (bool)
    {
        //transfer from (need allowance)
        rewardProviderTokenAllowance = rewardProviderTokenAllowance.add(
            _amount
        );
        uint256 amount = _amount * 1e9;
        tokenContract.transferFrom(msg.sender, address(this), amount);

        emit NativeTokenRewardAdded(msg.sender, _amount);
        return true;
    }

    function removeStakedTokenReward(uint256 _amount)
        public
        onlyRewardProvider
        returns (bool)
    {
        require(
            _amount <= rewardProviderTokenAllowance,
            "you cannot withdraw this amount"
        );
        rewardProviderTokenAllowance = rewardProviderTokenAllowance.sub(
            _amount
        );
        tokenContract.transfer(msg.sender, _amount);
        emit NativeTokenRewardRemoved(msg.sender, _amount);
        return true;
    }

    function _definePackage(
        string memory _name,
        uint256 _days,
        uint256 _daysBlocked,
        uint256 _packageInterest,
        uint256 _packageDefiReward
    ) public onlyOwnerAndAdmin {
        YieldType memory package;
        package._packageName = _name;
        package._daysLocked = _days;
        package._packageInterest = _packageInterest;
        package._packageDefiReward = _packageDefiReward;
        package._daysBlocked = _daysBlocked;
        packages[_name] = package;
        packageNames.push(_name);
    }

    address token0;
    address token1;
    uint256 startDateTime;
    uint256 endDateTime;

    function initialize(address _token0, address _token1, uint256 _startDateTime, uint256 _endDateTime,address _admin) external override{
        require(msg.sender == factory, 'DEFI: FORBIDDEN'); // sufficient check
        require(_startDateTime>0 && _endDateTime>0 && _endDateTime>_startDateTime, 'DEFI: Pool endTime must greater than starTime');
        token0 = _token0;
        token1 = _token1;
        startDateTime = _startDateTime;
        endDateTime = _endDateTime;
        tokenContract = IERC20(_token0);
        Defi = IERC20(_token1);
        admin = _admin;
        _setupRole(DEFAULT_ADMIN_ROLE, admin);

    }
}

contract DefiFactory {
    mapping(address => mapping(address => address)) public getPair;
    address[] public allPairs;
    struct poolInfo{
        uint256 startDateTime;
        uint256 endDateTime;
        address pairAddress;
        address token0Address;
        address token1Address;
    }
    poolInfo[] public pool;
    mapping(address => poolInfo) public getPairInfo;

    event PairCreated(address indexed token0, address indexed token1, address pair, uint);

    function allPairsLength() external view returns (uint) {
        return allPairs.length;
    }

    function createPair(address tokenA, address tokenB, uint256 _days) external returns (address pair) {
        // require(tokenA != tokenB, 'Defi: IDENTICAL_ADDRESSES');
        (address token0, address token1) = (tokenA, tokenB);
        uint256 startDateTime = block.timestamp;
        uint256 endDateTime = (_days * 1 days) + startDateTime;
        require(token0 != address(0), 'Defi: ZERO_ADDRESS');
        require(getPair[token0][token1] == address(0), 'Defi: PAIR_EXISTS'); // single check is sufficient
        bytes memory bytecode = type(DefiStaking).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(token0, token1));
        assembly {
            pair := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }
        address admin = msg.sender;
        IDefi(pair).initialize(token0, token1, startDateTime, endDateTime, admin);
        getPair[token0][token1] = pair;
        getPair[token1][token0] = pair; // populate mapping in the reverse direction
        allPairs.push(pair);
        getPairInfo[pair] = poolInfo(startDateTime,endDateTime,pair,token0, token1);
        pool.push(poolInfo(startDateTime,endDateTime,pair,token0, token1));
        emit PairCreated(token0, token1, pair, allPairs.length);
    }
}